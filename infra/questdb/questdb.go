package questdb

import (
	"database/sql"
	"fidel/config"
	"fmt"
	"log"
	"net"
)

var conn *net.TCPConn

var connStr = fmt.Sprintf("host=%s port=%s user=%s password=%s dbname=%s sslmode=disable", config.QUESTDB_HOST, config.QUESTDB_PORT_POSTGRES, config.QUESTDB_USER, config.QUESTDB_PASSWORD, config.QUESTDB_DATABASE)

type Column struct {
	Name string
	Type interface{}
}

type QueryResult struct {
	Query   string
	Columns []*Column
	Dataset []interface{}
	Count   int64
}

type QueryError struct {
	Query    string
	Error    string
	Position int64
}

func ConnectQuestdb() *sql.DB {

	db, err := sql.Open("postgres", connStr)
	CheckErr(err)

	return db
}

func CheckErr(err error) {
	if err != nil {
		panic(err)
	}
}

func ConnectQuestInflux() (*net.TCPConn, error) {

	host := "localhost:9009"
	tcpAddr, err := net.ResolveTCPAddr("tcp4", host)
	CheckErr(err)
	if conn != nil {
		return conn, nil
	}
	conn, err = net.DialTCP("tcp", nil, tcpAddr)
	CheckErr(err)

	return conn, nil
}

func InsertStream(data string) {
	if conn == nil {
		conn, err := ConnectQuestInflux()
		CheckErr(err)
		_, err = conn.Write([]byte(data))
		CheckErr(err)
		return
	}
	_, err := conn.Write([]byte(data))
	CheckErr(err)
}

func QueryData(query string) (*sql.Rows, error) {

	db := ConnectQuestdb()
	defer db.Close()
	log.Println(query)
	rows, err := db.Query(query)
	CheckErr(err)

	return rows, err

}
