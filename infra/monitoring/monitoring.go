package monitoring

import (
	"fidel/infra/questdb"
	"fmt"
	"time"

	"github.com/shirou/gopsutil/cpu"
	"github.com/shirou/gopsutil/disk"
	"github.com/shirou/gopsutil/mem"
)

func MonitoringComputer() {

	// memory

	for range time.Tick(5 * time.Second) {

		// Prints UTC time and date
		bucket := "resources"
		vmStat, _ := mem.VirtualMemory()
		mem_free := vmStat.Free
		mem_used := vmStat.Used
		mem_cached := vmStat.Cached
		diskStat, _ := disk.Usage("/")
		disk_percent := diskStat.UsedPercent
		percentage, _ := cpu.Percent(0, true)
		clocks_sec := cpu.ClocksPerSec
		n := float64(len(percentage))
		sum := 0.0
		for _, cpupercent := range percentage {
			sum = sum + cpupercent
		}
		avg := sum / n

		resources := fmt.Sprintf("%s mem_free=%d,mem_used=%d,mem_cached=%d,disk_usage=%f,clocks_sec=%f,cpu_usage_avg=%f", bucket, mem_free, mem_used, mem_cached, disk_percent, clocks_sec, avg)
		resources_line := resources + "\n"
		questdb.InsertStream(resources_line)

	}

}
