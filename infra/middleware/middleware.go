package middleware

import (
	"fidel/infra/questdb"
	"fidel/util"
	"fmt"
	"time"

	"github.com/gin-gonic/gin"
)

func LogMiddleware() gin.HandlerFunc {
	return func(c *gin.Context) {
		start := time.Now()
		c.Next()
		bucket := "requests"
		duration := util.GetDurationInMillseconds(start)
		client_ip := util.GetClientIP(c)
		method := c.Request.Method
		path := c.Request.RequestURI
		status := c.Writer.Status()
		requests := fmt.Sprintf("%s request_time=%f,client_ip=\"%s\",method=\"%s\",path=\"%s\",status=%d", bucket, duration, client_ip, method, path, status)
		requests_line := requests + "\n"
		questdb.InsertStream(requests_line)
	}
}
