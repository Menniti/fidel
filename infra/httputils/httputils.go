package httputils

import (
	"fidel/config"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
)

func GetUrl() string {
	url := fmt.Sprintf("http://%s:%s", config.QUESTDB_HOST, config.QUESTDB_PORT_QUERY_URL)
	return url
}

func GetRequest(path string) ([]byte, error) {
	resp, err := http.Get(path)
	if err != nil {
		log.Println(err)
		return nil, err
	}
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Printf("Error reading body: %v", err)
		return nil, err
	}
	return body, nil
}
