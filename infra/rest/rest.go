package rest

import (
	"fidel/infra/middleware"
	"net/http"

	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
)

func SetupRouter() *gin.Engine {
	// Disable Console Color
	// gin.DisableConsoleColor()
	r := gin.Default()
	r.Use(cors.New(cors.Config{
		AllowOrigins: []string{"*"},
		AllowMethods: []string{"*"},
		AllowHeaders: []string{"*"},
	}))
	r.Use(middleware.LogMiddleware())
	// Ping test
	r.GET("/ping", func(c *gin.Context) {
		c.String(http.StatusOK, "pong")
	})
	//TODO, not working
	// r.GET("/api/:query", func(c *gin.Context) {
	// 	query := c.Params.ByName("query")
	// 	path := fmt.Sprintf("%s/exec?query=%v", httputils.GetUrl(), query)
	// 	fmt.Println(path)

	// 	bytes, err := httputils.GetRequest(path)
	// 	if err != nil {
	// 		fmt.Println(err)
	// 		c.JSON(http.StatusBadRequest, gin.H{"err": err})
	// 		return
	// 	}
	// 	results := string(bytes)
	// 	fmt.Println(results)
	// 	c.JSON(http.StatusOK, gin.H{"results": results})
	// })

	return r
}
