
Requirements:

- go 1.17
- Docker version 20.10.10
- docker-compose version 1.26.0

Up the system
```

go mod tidy

go run main.go

docker-compose up

```
```
Dashboard QuestDB
http://localhost:9000/
```

Sample - Query from Questdb directly
```
http://localhost:9000/exec?query=SELECT * FROM resources;

http://localhost:9000/exec?query=SELECT * FROM transactions;

http://localhost:9000/exec?query=SELECT COUNT(*), SUM('transactions'.amount), AVG('transactions'.amount), countryId, cardId FROM 'transactions' WHERE 'transactions'.amount > 200;

http://localhost:9000/exec?query=SELECT timestamp, COUNT(*), SUM('transactions'.amount), AVG('transactions'.amount) FROM 'transactions' SAMPLE BY 1m

//export to CSV
http://localhost:9000/exp?query=SELECT * FROM transactions;
```

Picture of Google Draw

https://docs.google.com/drawings/d/1hqFDyYfWBgpW8pYhk21XREtTdyzPBdUYAlkNshXJvkQ/edit?usp=sharing