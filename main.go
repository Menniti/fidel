package main

import (
	"crypto/rand"
	"fidel/infra/monitoring"
	"fidel/infra/questdb"
	"fidel/infra/rest"
	"fmt"
	"math/big"

	"github.com/google/uuid"
)

func getNumber(value int64) int {
	nBig, err := rand.Int(rand.Reader, big.NewInt(value))
	if err != nil {
		panic(err)
	}
	n := int(nBig.Int64() + 1)
	return n
}

func addData() {
	for {
		bucket := "transactions"
		uuid := uuid.New().String()
		currency := "GBP"
		auth := "True"
		created := "2021-03-03T12:24:35.748Z"
		authCode := "099329"
		updated := "2021-03-03T12:24:36.102Z"
		amount := float64(getNumber(10000))
		cleared := "False"
		wallet := ""
		offer := ""
		cardId := getNumber(3)
		countryId := getNumber(3)
		brandId := getNumber(3)
		transactions := fmt.Sprintf("%s uuid=\"%s\",currency=\"%s\",auth=\"%s\",created=\"%s\",authCode=\"%s\",updated=\"%s\",amount=%f,cleared=\"%s\",wallet=\"%s\",offer=\"%s\",countryId=%d,cardId=%d,brandId=%d", bucket, uuid, currency, auth, created, authCode, updated, amount, cleared, wallet, offer, countryId, cardId, brandId)
		transactions_line := transactions + "\n"
		questdb.InsertStream(transactions_line)
	}
}

func main() {

	bucket := "cards"
	cardId := 1
	scheme := "visa"
	cards := fmt.Sprintf("%s id=%d,scheme=\"%s\"", bucket, cardId, scheme)
	cards_line := cards + "\n"
	questdb.InsertStream(cards_line)

	bucket = "cards"
	cardId = 2
	scheme = "master"
	cards = fmt.Sprintf("%s id=%d,scheme=\"%s\"", bucket, cardId, scheme)
	cards_line = cards + "\n"
	questdb.InsertStream(cards_line)

	bucket = "cards"
	cardId = 3
	scheme = "amex"
	cards = fmt.Sprintf("%s id=%d,scheme=\"%s\"", bucket, cardId, scheme)
	cards_line = cards + "\n"
	questdb.InsertStream(cards_line)

	bucket = "locations"
	country := "London"
	countryId := 1
	locations := fmt.Sprintf("%s id=%d,country=\"%s\"", bucket, countryId, country)
	locations_line := locations + "\n"
	questdb.InsertStream(locations_line)

	bucket = "locations"
	country = "German"
	countryId = 2
	locations = fmt.Sprintf("%s id=%d,country=\"%s\"", bucket, countryId, country)
	locations_line = locations + "\n"
	questdb.InsertStream(locations_line)

	bucket = "locations"
	country = "Portugal"
	countryId = 3
	locations = fmt.Sprintf("%s id=%d,country=\"%s\"", bucket, countryId, country)
	locations_line = locations + "\n"
	questdb.InsertStream(locations_line)

	bucket = "brands"
	brandId := 1
	name := "Brand 1"
	brands := fmt.Sprintf("%s id=%d,name=\"%s\"", bucket, brandId, name)
	brands_line := brands + "\n"
	questdb.InsertStream(brands_line)

	bucket = "brands"
	brandId = 2
	name = "Brand 2"
	brands = fmt.Sprintf("%s id=%d,name=\"%s\"", bucket, brandId, name)
	brands_line = brands + "\n"
	questdb.InsertStream(brands_line)

	bucket = "brands"
	brandId = 3
	name = "Brand 3"
	brands = fmt.Sprintf("%s id=%d,name=\"%s\"", bucket, brandId, name)
	brands_line = brands + "\n"
	questdb.InsertStream(brands_line)

	go addData()
	go monitoring.MonitoringComputer()

	r := rest.SetupRouter()
	r.Run(":9091")
}
